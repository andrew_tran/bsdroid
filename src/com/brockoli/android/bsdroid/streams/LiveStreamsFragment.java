/*
   Copyright [2012] [Rob Woods]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.brockoli.android.bsdroid.streams;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import com.brockoli.android.bsdroid.HSDroidSettings;
import com.brockoli.android.bsdroid.LoginActivity;
import com.brockoli.android.bsdroid.MainActivity;
import com.brockoli.android.bsdroid.R;
import com.brockoli.android.bsdroid.datamodel.BaseStream;
import com.brockoli.android.bsdroid.datamodel.LiveStream;
import com.brockoli.android.bsdroid.service.RESTService;
import com.brockoli.android.bsdroid.streams.StreamsFragment.OnStreamButtonClickedListener;

/**
 * This fragment implements the Live Streams view and handles the JSON returned when we 
 * call the network for a list of Live Streams
 * 
 * @author brockoli
 *
 */
public class LiveStreamsFragment extends StreamsFragment implements OnStreamButtonClickedListener,
ExpandableListView.OnGroupExpandListener {
	private int mLastExpandedGroupId = -1;

	private Menu mMenu;
	
	private BroadcastReceiver mReciever = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle extras = intent.getExtras();
			
			if (intent.hasExtra(MainActivity.BROADCAST_INTENT_UNSUPPORTED_CHECKBOX)) {
				ExpandableStreamAdapter adapter = (ExpandableStreamAdapter) mLv.getExpandableListAdapter();
				if (adapter != null) {
					adapter.setSupportedStreams(intent.getBooleanExtra(MainActivity.BROADCAST_INTENT_UNSUPPORTED_CHECKBOX, false));					
				}
			}
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		setRetainInstance(true);
		
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mReciever, new IntentFilter("hsdroid-unsupported-checkbox"));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View showsView = inflater.inflate(R.layout.live_streams, null);

		mLv = (ExpandableListView) showsView.findViewById(R.id.stream_list);
		mLv.setOnGroupExpandListener(this);


		return showsView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Set the list adapter if coming from a savedInstance
		if (savedInstanceState != null) {
			mLastExpandedGroupId = savedInstanceState.getInt(LAST_EXPANDED_GROUP_ID);
			List streams = savedInstanceState.getParcelableArrayList(PREVIOUS_LIST_DATA);
			if (streams != null) {
				ExpandableStreamAdapter adapter = new ExpandableStreamAdapter(getActivity(), streams, true);
				adapter.setOnStreamButtonClickedListener(this);
				mLv.setAdapter(adapter);				
			}
		} else {
			ProgressBar loading = (ProgressBar) getActivity().findViewById(R.id.loading_streams);
			loading.setVisibility(View.VISIBLE);

			new LiveStreamsAsyncTask().execute(RESTService.instance().createRestIntent(RESTService.REST_METHOD_GET_LIVE, null, RESTService.GET));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(LAST_EXPANDED_GROUP_ID, mLastExpandedGroupId);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.live_menu, menu);
		mMenu = menu;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// handle item selection
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			item.setEnabled(false);
			mLv.setAdapter((ExpandableListAdapter)null);
			ProgressBar loading = (ProgressBar) getActivity().findViewById(R.id.loading_streams);
			loading.setVisibility(View.VISIBLE);
			new LiveStreamsAsyncTask().execute(RESTService.instance().createRestIntent(RESTService.REST_METHOD_GET_LIVE, null, RESTService.GET));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void showGameOverDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.dialog_title_event_is_over);
		builder.setMessage(R.string.dialog_msg_game_over)
		.setPositiveButton(getActivity().getString(android.R.string.ok), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}

	private class LiveStreamsAsyncTask extends StreamsAsyncTask {

		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				List streams = new ArrayList<LiveStream>();
				try {
					JSONObject jsonResponse = (JSONObject) new JSONTokener(result).nextValue();
					if (jsonResponse.getString("status").equals("Success")) {
						JSONArray jsonStreams = jsonResponse.getJSONArray("schedule");
						for (int i=0; i < jsonStreams.length(); i++) {
							JSONObject jsonStream = jsonStreams.getJSONObject(i);
							String id = jsonStream.getString("id");
							String event = jsonStream.getString("event");
							String homeTeam = jsonStream.getString("homeTeam");
							String awayTeam = jsonStream.getString("awayTeam");
							String startTime = jsonStream.getString("startTime");
							int playing = jsonStream.getInt("isPlaying");
							Boolean isPlaying = (playing == 0) ? false : true;
							int flash = jsonStream.getInt("isFlash");
							Boolean isFlash = (flash == 0) ? false : true;
							int wmv = jsonStream.getInt("isWMV");
							Boolean isWmv = (wmv == 0) ? false : true;
							int istream = jsonStream.getInt("isiStream");
							Boolean isIStream = (istream == 0) ? false : true;
							int hd = jsonStream.getInt("isHd");
							Boolean isHd = (hd == 0) ? false : true;
							LiveStream liveStream = new LiveStream(id, event, homeTeam, awayTeam, 
									isFlash, isWmv, isIStream, startTime, isPlaying, isHd);
							streams.add(liveStream);
						}
					} else {
						// Most likely failure here is your session token has expired, so kick back to login screen
						// Need to check error more closely and act appropriately but for now just show a toast
						if (jsonResponse.getString("msg").toLowerCase().contains("premium")) {
							showNonPremiumDialog();
						} else {
							showSessionTimeoutDialog();							
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				ExpandableStreamAdapter adapter = new ExpandableStreamAdapter(getActivity(), streams, true);
				adapter.setSupportedStreams(HSDroidSettings.getInstance().unsupportedStreamsFlag(getActivity()));
				adapter.setOnStreamButtonClickedListener(LiveStreamsFragment.this);
				mLv.setAdapter(adapter);								
			}
			
			// Hide the progress indicator regardless of result
			ProgressBar loading = (ProgressBar) getActivity().findViewById(R.id.loading_streams);
			loading.setVisibility(View.GONE);

			// Enable the refresh button
			MenuItem item = mMenu.findItem(R.id.menu_refresh);
			item.setEnabled(true);
		}
	}

	private void showSessionTimeoutDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Dialog));
		builder.setMessage(R.string.msg_dialog_session_timed_out)
		.setTitle(R.string.title_dialog_session_timed_out);

		builder.setPositiveButton(R.string.btn_login, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivity(intent);				
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	private void showNonPremiumDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Non-Premium account");
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.nonpremiumdialog, null);
		builder.setView(view);

		builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	@Override
	public void onGroupExpand(int groupPosition) {
		if (mLastExpandedGroupId < mLv.getCount() && mLastExpandedGroupId != groupPosition) {
			mLv.collapseGroup(mLastExpandedGroupId);
			mLastExpandedGroupId = groupPosition;
		}		
	}

	@Override
	public void onStreamButtonClicked(BaseStream stream, String type, Boolean isHd) {
		// Toast.makeText(getActivity(), "type: " + type + " id: " + stream.getId(), Toast.LENGTH_SHORT).show();

		// Slight hack here to pass the selected stream format to our AsyncTask so we can filter on it
		// Create a second intent with the stream type as an extra and pass it to our AsyncTask along
		// with the intent created for the REST service

		Intent streamTypeIntent = new Intent();
		streamTypeIntent.putExtra(STREAM_TYPE, type);
		streamTypeIntent.putExtra(STREAM_QUALITY, isHd);

		if (((LiveStream) stream).getStartTime().equalsIgnoreCase("Final")) {
			showGameOverDialog();
		} else {
			ContentValues cv = new ContentValues();
			cv.put(RESTService.REST_PARAM_STREAM_ID, stream.getId());

			// Check settings to see if a preferred server location is set and use it
			boolean useLocation = HSDroidSettings.getInstance().locationFlag(getActivity());
			String location;
			if (useLocation) {
				location = HSDroidSettings.getInstance().location(getActivity());
				cv.put(RESTService.REST_PARAM_LOCATION, location);
			}

			new StreamAsyncTask().execute(RESTService.instance()
					.createRestIntent(RESTService.REST_METHOD_GET_LIVE_STREAM, cv, RESTService.GET), streamTypeIntent);					
		}
	}
}
