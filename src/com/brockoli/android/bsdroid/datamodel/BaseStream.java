/*
   Copyright [2012] [Rob Woods]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.brockoli.android.bsdroid.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

public class BaseStream implements Parcelable {
	private static final String TAG = BaseStream.class.getSimpleName();
	private String mId;
	private String mEvent;
	private String mHomeTeam;
	private String mAwayTeam;
	private Boolean mFlash = false;
	private Boolean mWmv = false;
	private Boolean mIStream = false;
	private Boolean mHd = false;

	// Default constructor
	public BaseStream(String id, String event, String home, String away, Boolean flash, Boolean wmv, Boolean istream, Boolean hd) {
		this.mId = id;
		this.mEvent = event;
		this.mHomeTeam = home;
		this.mAwayTeam = away;
		this.mFlash = flash;
		this.mWmv = wmv;
		this.mIStream = istream;
		this.mHd = hd;
	}

	// Used by Parcelable.Creator
	public BaseStream(Parcel source) {
		//Log.v(TAG, "BaseStream(Parcel source): build from Parcel");
		this.mId = source.readString();
		this.mEvent = source.readString();
		this.mHomeTeam = source.readString();
		this.mAwayTeam = source.readString();
		this.mFlash = source.readByte() == 1;
		this.mWmv = source.readByte() == 1;
		this.mIStream = source.readByte() == 1;
		this.mHd = source.readByte() == 1;
	}

	public void setId(String id) {
		this.mId = id;
	}

	public String getId() {
		return this.mId;
	}

	public void setEvent(String event) {
		this.mEvent = event;
	}

	public String getEvent() {
		return this.mEvent;
	}

	public void setHomeTeam(String home) {
		this.mHomeTeam = home;
	}

	public String getHomeTeam() {
		return this.mHomeTeam;
	}

	public void setAwayTeam(String away) {
		this.mAwayTeam = away;
	}

	public String getAwayTeam() {
		return this.mAwayTeam;
	}

	public void setWmv(Boolean isAvailable) {
		this.mWmv = isAvailable;
	}

	public Boolean isWmvAvailable() {
		return this.mWmv;
	}

	public void setIStream(Boolean isAvailable) {
		this.mIStream = isAvailable;
	}

	public Boolean isIStreamAvailable() {
		return this.mIStream;
	}

	public void setFlash(Boolean isAvailable) {
		this.mFlash = isAvailable;
	}

	public Boolean isFlashAvailable() {
		return this.mFlash;
	}

	public void setHd(Boolean isHd) {
		this.mHd = isHd;
	}

	public Boolean isHd() {
		return this.mHd;
	}

	@Override
	public int describeContents() {
		return hashCode();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		//Log.v(TAG, "writeToParcel" + flags);
		dest.writeString(mId);
		dest.writeString(mEvent);
		dest.writeString(mHomeTeam);
		dest.writeString(mAwayTeam);
		dest.writeByte((byte) (mFlash ? 1 : 0));
		dest.writeByte((byte) (mWmv ? 1 : 0));
		dest.writeByte((byte) (mIStream ? 1 : 0));
		dest.writeByte((byte) (mHd ? 1 : 0));
	}

	public static final Parcelable.Creator<BaseStream> CREATOR
			= new Parcelable.Creator<BaseStream>() {
		
		@Override
		public BaseStream createFromParcel(Parcel source) {
			return new BaseStream(source);
		}

		@Override
		public BaseStream[] newArray(int size) {
			return new BaseStream[size];
		}
	};
}
