/*
   Copyright [2012] [Rob Woods]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.brockoli.android.bsdroid.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

public class LiveStream extends BaseStream {

	private static final String TAG = LiveStream.class.getSimpleName();
	private String mStartTime;
	private Boolean mPlaying;

	public LiveStream(String id, String event, String home, String away, Boolean flash, Boolean wmv, Boolean istream, Boolean isHd) {
		super(id, event, home, away, flash, wmv, istream, isHd);
	}

	public LiveStream(String id, String event, String home, String away, 
			Boolean flash, Boolean wmv, Boolean istream, String startTime, Boolean playing, Boolean isHd) {
		super(id, event, home, away, flash, wmv, istream, isHd);
		this.mStartTime = startTime;
		this.mPlaying = playing;
	}

	// Used by Parcelable.Creator
	public LiveStream(Parcel source) {
		super(source);
		this.mStartTime = source.readString();
		this.mPlaying = source.readByte() == 1;
	}

	public void setStartTime(String startTime) {
		this.mStartTime = startTime;
	}

	public String getStartTime() {
		return this.mStartTime;
	}

	public void setPlaying(Boolean playing) {
		this.mPlaying = playing;
	}

	public Boolean isPlaying() {
		return this.mPlaying;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		//Log.v(TAG, "writeToParcel" + flags);
		super.writeToParcel(dest, flags);
		dest.writeString(mStartTime);
		dest.writeByte((byte) (mPlaying ? 1 : 0));
	}

	public static final Parcelable.Creator<LiveStream> CREATOR 
			= new Parcelable.Creator<LiveStream>() {
		
		@Override
		public LiveStream createFromParcel(Parcel source) {
			return new LiveStream(source);
		}

		@Override
		public LiveStream[] newArray(int size) {
			return new LiveStream[size];
		}
	};
}
