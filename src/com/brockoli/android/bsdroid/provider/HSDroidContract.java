package com.brockoli.android.bsdroid.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public class HSDroidContract {
    public static final String AUTHORITY = "com.brockoli.android.hsdroid.provider.bsdroidcontract";

    /**
     * Favorite teams table contract
     */
    public static final class Teams implements BaseColumns {
        // This class cannot be instantiated
        private Teams() {
        }

        /**
         * The table name offered by this provider
         */
        public static final String TABLE_NAME = "teams";

        /*
         * URI definitions
         */

        /**
         * The scheme part for this provider's URI
         */
        private static final String SCHEME = "content://";

        /**
         * Path parts for the URIs
         */

        /**
         * Path part for the Teams URI
         */
        private static final String PATH_TEAMS = "/teams";

        /**
         * Path part for the Teams ID URI
         */
        private static final String PATH_TEAMS_ID = "/teams/";

        /**
         * 0-relative position of a team ID segment in the path part of a team ID URI
         */
        public static final int TEAMS_ID_PATH_POSITION = 1;

        /**
         * The content:// style URL for this table
         */
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_TEAMS);

        /**
         * The content URI base for a single team. Callers must append a numeric id to this Uri to
         * retrieve a team
         */
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_TEAMS_ID);

        /**
         * The MIME type of {@link #CONTENT_URI} providing a directory of teams.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hsdroid.teams";

        /**
         * The MIME type of a {@link #CONTENT_URI} sub-directory of a single team.
         */
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hsdroid.teams";

        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER = TABLE_NAME + ".modified ASC";

        /*
         * Column definitions
         */

        /**
         * Column name for team name, 
         * <P>
         * Type: TEXT
         * </P>
         */
        public static final String COLUMN_NAME_TEAM_NAME = "name";

        /**
         * Column name for the creation timestamp
         * <P>
         * Type: INTEGER (long from System.curentTimeMillis())
         * </P>
         */
        public static final String COLUMN_NAME_CREATE_DATE = "created";

        /**
         * Column name for the modification timestamp
         * <P>
         * Type: INTEGER (long from System.curentTimeMillis())
         * </P>
         */
        public static final String COLUMN_NAME_MODIFICATION_DATE = "modified";
    }
    
    /**
     * Notifications table contract
     */
    public static final class Notifications implements BaseColumns {
        // This class cannot be instantiated
        private Notifications() {
        }

        /**
         * The table name offered by this provider
         */
        public static final String TABLE_NAME = "notifications";

        /*
         * URI definitions
         */

        /**
         * The scheme part for this provider's URI
         */
        private static final String SCHEME = "content://";

        /**
         * Path parts for the URIs
         */

        /**
         * Path part for the Notifications URI
         */
        private static final String PATH_NOTIFICATIONS = "/notifications";

        /**
         * Path part for the Notifications ID URI
         */
        private static final String PATH_NOTIFICATIONS_ID = "/notifications/";

        /**
         * 0-relative position of a notification ID segment in the path part of a notification ID URI
         */
        public static final int NOTIFICATIONS_ID_PATH_POSITION = 1;

        /**
         * The content:// style URL for this table
         */
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_NOTIFICATIONS);

        /**
         * The content URI base for a single notification. Callers must append a numeric id to this Uri to
         * retrieve a notification
         */
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_NOTIFICATIONS_ID);

        /**
         * The MIME type of {@link #CONTENT_URI} providing a directory of notification.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hsdroid.notifications";

        /**
         * The MIME type of a {@link #CONTENT_URI} sub-directory of a single notification.
         */
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hsdroid.notifications";

        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER = TABLE_NAME + ".modified ASC";

        /*
         * Column definitions
         */

        /**
         * Column name for notification id, 
         * <P>
         * Type: INTEGER
         * </P>
         */
        public static final String COLUMN_NAME_NOTIFICATION_ID = "id";

        /**
         * Column name for the creation timestamp
         * <P>
         * Type: INTEGER (long from System.curentTimeMillis())
         * </P>
         */
        public static final String COLUMN_NAME_CREATE_DATE = "created";

        /**
         * Column name for the modification timestamp
         * <P>
         * Type: INTEGER (long from System.curentTimeMillis())
         * </P>
         */
        public static final String COLUMN_NAME_MODIFICATION_DATE = "modified";
    }
}
