package com.brockoli.android.bsdroid.provider;

import java.util.HashMap;
import java.util.Map;

import android.content.ContentResolver;

/** Fluent interface for construction of contract columns to database columns. */
public class ProjectionBuilder {

    private final Map<String, String> mProjectionMap;
    private String mTable;

    public ProjectionBuilder() {

        mProjectionMap = new HashMap<String, String>();
    }

    /** Sets a default table name for all {@code add} operations. */
    public ProjectionBuilder setTable(String databaseTable) {
        mTable = databaseTable;

        return this;
    }

    /**
     * Adds a mapping from {@code projectionName} to a fully qualified database table/column.
     * 
     * @param projectionName
     *            {@link ContentResolver} contract column name
     * @param databaseTable
     *            table name in which {@code databaseColumn} is located
     * @param databaseColumn
     *            column to be retrieved
     * 
     * @return builder
     */
    public ProjectionBuilder add(String projectionName, String databaseTable, String databaseColumn) {

        mProjectionMap.put(projectionName, databaseTable + "." + databaseColumn);

        return this;
    }

    /**
     * Adds a mapping from {@code projectionName} to column. If {@link #setTable(String)} has been called, the column
     * will be qualified with the table name.
     * 
     * @param projectionName
     *            {@link ContentResolver} contract column name
     * @param databaseColumn
     *            column to be retrieved
     * 
     * @return builder
     */
    public ProjectionBuilder add(String projectionName, String databaseColumn) {

        if (mTable != null) {
            add(projectionName, mTable, databaseColumn);
        } else {
            mProjectionMap.put(projectionName, databaseColumn);
        }

        return this;
    }

    /**
     * Adds a mapping from {@code projectionAndColumnName} to a column with the same name. If {@link #setTable(String)}
     * has been called, the column will be qualified with the table name.
     * 
     * @param projectionNameAndColumnName
     *            {@link ContentResolver} contract and database column name
     * 
     * @return builder
     */
    public ProjectionBuilder add(String projectionAndColumnName) {

        return add(projectionAndColumnName, projectionAndColumnName);
    }

    /**
     * <p>
     * Adds a mapping from {@code projectionName} to a column with {@code databaseColumn} <i>and</i> renames the
     * returned column to {@code projectionName}.
     * </p>
     * <p>
     * e.g. if {@code foo} is passed in as the projection name, and {@code bar} was is its mapped database column, a
     * query would return a cursor with a column named {@code foo}.
     * </p>
     * 
     * @param projectionName
     *            projection name, and name of the column in the query's cursor
     * @param databaseColumn
     *            database column mapped to projection name
     * 
     * @return builder
     */
    public ProjectionBuilder alias(String projectionName, String databaseColumn) {

        String databaseColumnWithAlias = databaseColumn + " AS " + projectionName;

        return add(projectionName, databaseColumnWithAlias);
    }

    /**
     * Returns the projection mapping.
     * 
     * @return mapping of contract column names to database column names
     */
    public Map<String, String> build() {
        return mProjectionMap;
    }
}
