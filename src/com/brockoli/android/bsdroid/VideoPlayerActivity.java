/*
   Copyright [2012] [Rob Woods]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package com.brockoli.android.bsdroid;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup.LayoutParams;
import android.widget.MediaController;
import android.widget.ProgressBar;

import com.brockoli.android.bsdroid.R;

public class VideoPlayerActivity extends Activity implements MediaPlayer.OnPreparedListener, 
															 MediaPlayer.OnVideoSizeChangedListener, 
															 SurfaceHolder.Callback,
															 MediaController.MediaPlayerControl,
															 OnSeekCompleteListener,
															 OnBufferingUpdateListener,
															 OnErrorListener {

	private MediaPlayer mp;
	private MediaController mc;
	private int width = 0;
	private int height = 0;
	private SurfaceView surface;
	private SurfaceHolder holder;
	private ProgressBar timeline = null;
	private boolean isPaused = false;
	private int mCurrentPosition = 0;
	
	private Handler handler = new Handler();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.video_player);
	
		if (getLastNonConfigurationInstance() != null) {
			mCurrentPosition = (Integer) getLastNonConfigurationInstance();			
		}
		
		mc = new MediaController(this);
		
		
		surface = (SurfaceView) findViewById(R.id.surface_video);
		holder = surface.getHolder();
		holder.addCallback(this);

	}

	@Override
	protected void onPause() {
		super.onPause();
		
		isPaused = true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		width = getScreenWidth();
		height = setHeightBasedOnWidth();
		
		isPaused = false;
		
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		if (mp != null) {
			mp.release();
			mp = null;
		}
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		int currentPosition = 0;
		if (mp != null && mp.isPlaying()) {
			currentPosition = mp.getCurrentPosition();
		}
		return currentPosition;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mc.show();
		return false;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Intent intent = this.getIntent();
		String url = intent.getStringExtra("STREAM_ID");

		playVideo(url);
	
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		
	}
	
	@Override
	public void onPrepared(MediaPlayer mediaPlayer) {
		LayoutParams lp = surface.getLayoutParams();
		lp.width = width;
		lp.height = height;
		surface.setLayoutParams(lp);
		if (width != 0 && height != 0) {
			holder.setFixedSize(width, height);
			mc.setMediaPlayer(this);
			mc.setAnchorView(surface);
			
			handler.post(new Runnable() {
				
				@Override
				public void run() {
					mc.setEnabled(true);
					mc.show();
				}
			});

			Log.d("MEDIAPLAYER", "onPrepared start");
			mediaPlayer.seekTo(mCurrentPosition);
			mediaPlayer.start();
		}
	}
	
	private void playVideo(String url) {
			try {
				if (mp == null) {
					mp = new MediaPlayer();
					mp.setScreenOnWhilePlaying(true);
					mp.setOnVideoSizeChangedListener(this);
				} else {
					mp.stop();
					mp.reset();
				}
				
				mp.setDataSource(url);
				mp.setDisplay(holder);
				mp.setOnSeekCompleteListener(this);
				mp.setOnBufferingUpdateListener(this);
				mp.setOnErrorListener(this);
				mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
				mp.setOnPreparedListener(this);
				mp.prepareAsync();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void onVideoSizeChanged(MediaPlayer mediaPlayer, int newWidth, int newHeight) {
/*		if (width != 0 && height != 0) {
			holder.setFixedSize(width, height);
			//mp.seekTo(mCurrentPosition);
			Log.d("MEDIAPLAYER", "onVideoSizeChanged start");
			//mediaPlayer.start();
		}
*/		
	}
	
	private Point getScreenSize() {
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		return size;
	}
	
	private int getScreenWidth() {
		return getScreenSize().x;
	}
	
	private int getScreenHeight() {
		return getScreenSize().y;
	}
	
	private int setHeightBasedOnWidth() {
		if (getScreenWidth() > getScreenHeight()) {
			return getScreenHeight();
		}
		return (int) (getScreenWidth()/1.6);
	}
	
	@Override
	public boolean canPause() {
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		return true;
	}

	@Override
	public boolean canSeekForward() {
		return true;
	}

	@Override
	public int getBufferPercentage() {
		return 0;
	}

	@Override
	public int getCurrentPosition() {
		return mp.getCurrentPosition();
	}

	@Override
	public int getDuration() {
		return mp.getDuration();
	}

	@Override
	public boolean isPlaying() {
		return mp.isPlaying();
	}

	@Override
	public void pause() {
		mp.pause();
	}

	@Override
	public void seekTo(int pos) {
		mp.seekTo(pos);
	}

	@Override
	public void start() {
		Log.d("MEDIAPLAYER", "MediaController start");
		mp.start();
	}

	@Override
	public void onSeekComplete(MediaPlayer mp) {
		mp.start();
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {
		int buffered = mp.getDuration()*(percent/100);
		timeline.setSecondaryProgress(buffered);
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.d("MyAudioView", "Error: " + what + "," + extra);
        if (mc != null) {
            mc.hide();
        }

        /* Otherwise, pop up an error dialog so the user knows that
         * something bad has happened. Only try and pop up the dialog
         * if we're attached to a window. When we're going away and no
         * longer have a window, don't bother showing the user an error.
         */
            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("This stream could not be played")
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    /* If we get here, there is no onError listener, so
                                     * at least inform them that the video is over.
                                     */
                                }
                            })
                    .setCancelable(false)
                    .show();
        return true;
	}
}
