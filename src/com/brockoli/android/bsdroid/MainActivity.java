/*
   Copyright [2012] [Rob Woods]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.brockoli.android.bsdroid;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.PendingIntent;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.CursorAdapter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.brockoli.android.bsdroid.R;
import com.brockoli.android.bsdroid.provider.HSDroidContract;
import com.brockoli.android.bsdroid.service.OnAlarmReceiver;
import com.brockoli.android.bsdroid.service.OnClearNotificationsReceiver;
import com.brockoli.android.bsdroid.service.RESTService;
import com.brockoli.android.bsdroid.streams.LiveStreamsFragment;
import com.brockoli.android.bsdroid.streams.OnDemandStreamsFragment;
import com.brockoli.android.bsdroid.utils.DBUtils;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingFragmentActivity;


public class MainActivity extends SlidingFragmentActivity implements ViewPager.OnPageChangeListener,
LoaderCallbacks<Cursor>{

	private static final String CURRENT_PAGE = "CURRENT_PAGE";
	private static final int PERIOD = 86400000;  // 1 day
	private static final String LOCATIONS_LIST = "LOCATIONS_LIST";
	
	public static final String BROADCAST_INTENT_UNSUPPORTED_CHECKBOX = "UNSUPPORTED_CHECKBOX";

	
	private LocationsAdapter mLocationsAdapter;

	StreamsPagerAdapter streamsPagerAdapter;
	FavoriteTeamsAdapter mFavoriteTeamsAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	private int mCurrentPage = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setBehindContentView(R.layout.menu);

		// setup the sliding menu
		setupSlidingMenu();
		
		// setup app settings
		setupSettings();

		ListView lv = (ListView) findViewById(R.id.fav_list);
		mFavoriteTeamsAdapter = new FavoriteTeamsAdapter(this, null, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		lv.setAdapter(mFavoriteTeamsAdapter);

		getLoaderManager().initLoader(0, null, this);

		// LivePagerAdapter will only ever return 1 page
		streamsPagerAdapter = new StreamsPagerAdapter(getSupportFragmentManager());

		// Set up the ExtendedViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(streamsPagerAdapter);
		mViewPager.setOnPageChangeListener(this);

		// setup the ActionBar
		if (savedInstanceState != null) {
			mCurrentPage = savedInstanceState.getInt(CURRENT_PAGE);
			
			ArrayList<String> locationsList = savedInstanceState.getStringArrayList(LOCATIONS_LIST);
			if (locationsList != null && locationsList.isEmpty()) {
				mLocationsAdapter = new LocationsAdapter(locationsList);
				setSpinnerLocation();
			} else {
				// rest call to get locations
				new LocationsAsyncTask().execute(RESTService.instance()
						.createRestIntent(RESTService.REST_METHOD_LIST_LOCATIONS, null, RESTService.GET));						
			}
		} else {
			// rest call to get locations
			new LocationsAsyncTask().execute(RESTService.instance()
					.createRestIntent(RESTService.REST_METHOD_LIST_LOCATIONS, null, RESTService.GET));						
		}
		setupActionBar();
		setupNotificationServiceAlarm();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(CURRENT_PAGE, mCurrentPage);
		
		if (mLocationsAdapter != null && !mLocationsAdapter.isEmpty()) {
			outState.putStringArrayList(LOCATIONS_LIST, mLocationsAdapter.getList());
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			toggle();
			return true;
		case R.id.menu_ip_exception:
			new IPExceptionAsyncTask().execute(RESTService.instance()
					.createRestIntent(RESTService.REST_METHOD_IP_EXCEPTION, null, RESTService.POST));			
			return true;
		case R.id.menu_logout:
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);	
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void setupActionBar() {
		ActionBar ab = getActionBar();
		ab.setTitle(R.string.app_name);

		if (mCurrentPage == 1) {
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		}
		ab.setHomeButtonEnabled(true);    	
		ab.setDisplayHomeAsUpEnabled(true);
	}

	private void setupSlidingMenu() {
		// customize the SlidingMenu
		this.setSlidingActionBarEnabled(false);
		SlidingMenu menu = getSlidingMenu();
		menu.setBehindOffsetRes(R.dimen.actionbar_home_width);
		menu.setBehindScrollScale(0.25f);
		menu.setShadowDrawable(R.drawable.defaultshadow);
		menu.setShadowWidth(12);	
	}

	private void setupSettings() {
		// Set useLocation switch from app preferences
		Switch locSwitch = (Switch) findViewById(R.id.useLocation);
		locSwitch.setChecked(HSDroidSettings.getInstance().locationFlag(this));
		
		if (mLocationsAdapter != null && !mLocationsAdapter.isEmpty()) {
			setSpinnerLocation();
		}
		
		CheckBox cbUnsupportedStreams = (CheckBox) findViewById(R.id.hide_streams);
		
		cbUnsupportedStreams.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				HSDroidSettings.getInstance().setUnsupportedStreamsFlag(MainActivity.this, isChecked);
				
				// Broadcast an intent indicating that the checked state has changed so our
				// Streams fragment can recieve it and update it's adapter
				Intent intent = new Intent("hsdroid-unsupported-checkbox");
				intent.putExtra(BROADCAST_INTENT_UNSUPPORTED_CHECKBOX, isChecked);
				LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
			}
		});
		
		cbUnsupportedStreams.setChecked(HSDroidSettings.getInstance().unsupportedStreamsFlag(this));
	}
	
	private void setSpinnerLocation() {
		Spinner locSpinner = (Spinner) findViewById(R.id.locations);
		String location = HSDroidSettings.getInstance().location(MainActivity.this);
		int position = mLocationsAdapter.getPosition(location);
		locSpinner.setSelection(position);		
	}
	
	/**
	 * AsyncTask that calls the REST service to get the list of server locations
	 *
	 */
	protected class LocationsAsyncTask extends AsyncTask<Intent, Void, String> {

		@Override
		protected String doInBackground(Intent... params) {
			String body = null;
			HttpResponse response = RESTService.request(params[0]);
			if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				try {
					body = EntityUtils.toString(response.getEntity());
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}				
			}

			return body;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				ArrayList<String> locationsList = new ArrayList<String>();
				try {
					JSONArray locations = (JSONArray) new JSONTokener(result).nextValue();
					if (locations != null) {
						for (int i=0; i<locations.length(); i++) {
							JSONObject location = locations.getJSONObject(i);
							locationsList.add(location.getString("location"));
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (locationsList != null && !locationsList.isEmpty()) {
					mLocationsAdapter = new LocationsAdapter(locationsList);
					
					setupLocationSpinner();
				}
			}
		}		
	}

	private void setupLocationSpinner() {
		final Spinner locSpinner = (Spinner) findViewById(R.id.locations);
		Switch btnLoc = (Switch) findViewById(R.id.useLocation);
		btnLoc.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					locSpinner.setEnabled(true);
					HSDroidSettings.getInstance().setLocationFlag(MainActivity.this, true);
				} else {
					locSpinner.setEnabled(false);
					HSDroidSettings.getInstance().setLocationFlag(MainActivity.this, false);
				}
			}
		});
		if (mLocationsAdapter != null && !mLocationsAdapter.isEmpty()) {
			locSpinner.setAdapter(mLocationsAdapter);
		}
		
		locSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				HSDroidSettings.getInstance().setLocation(MainActivity.this, (String)mLocationsAdapter.getItem(position));
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		
		locSpinner.setEnabled(btnLoc.isChecked());
		setSpinnerLocation();
	}
	
	private class LocationsAdapter implements SpinnerAdapter {

		ArrayList<String> data;

		public LocationsAdapter(ArrayList<String> locations) {
			this.data = locations;
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public int getItemViewType(int position) {
			return android.R.layout.simple_spinner_dropdown_item;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView v = new TextView(MainActivity.this);
			v.setTextColor(getResources().getColor(android.R.color.black));
			v.setTextSize(18.0f);
			v.setTypeface(null, Typeface.BOLD);
			v.setText(data.get(position));
			return v;
		}

		@Override
		public int getViewTypeCount() {
			return 1;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public boolean isEmpty() {
			return data.isEmpty();
		}

		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
			// TODO Auto-generated method stub

		}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {

		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			TextView v = new TextView(MainActivity.this);
			v.setTextColor(getResources().getColor(android.R.color.black));
			v.setBackgroundColor(getResources().getColor(R.color.hs_purple));
			v.setTextSize(18.0f);
			v.setText(data.get(position));
			return v;
		}

		public ArrayList<String> getList() {
			return data;
		}
		
		public int getPosition(String value) {
			for (int i=0; i < data.size(); i++) {
				if (data.get(i).equals(value)) {
					return i;
				}
			}
			return 4;  // North America - East
		}
	}
	
	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
	 * sections of the app.
	 */
	public class StreamsPagerAdapter extends FragmentPagerAdapter {

		private static final int MAX_PAGE_COUNT = 10;
		private int mPagerId = 1;

		public StreamsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {
			switch (i) {
			case 0 :
				return new LiveStreamsFragment();
			case 1 :
				return new OnDemandStreamsFragment();
			default :
				Fragment fragment = new DummySectionFragment();
				Bundle args = new Bundle();
				args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
				fragment.setArguments(args);
				return fragment;        			
			}
		}

		@Override
		public int getCount() {
			return 2;
		}

		@Override
		public long getItemId(int position) {
			return mPagerId*MAX_PAGE_COUNT+position;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0: return getString(R.string.btn_live_streams).toUpperCase();
			case 1: return getString(R.string.btn_on_demand_streams).toUpperCase();
			}
			return null;
		}		
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		public DummySectionFragment() {
		}

		public static final String ARG_SECTION_NUMBER = "section_number";

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			TextView textView = new TextView(getActivity());
			textView.setGravity(Gravity.CENTER);
			Bundle args = getArguments();
			textView.setText(Integer.toString(args.getInt(ARG_SECTION_NUMBER)));
			return textView;
		}
	}

	/**
	 * AsyncTask that calls the REST service to get an IP exception
	 * @author brockoli
	 *
	 */
	protected class IPExceptionAsyncTask extends AsyncTask<Intent, Void, String> {

		@Override
		protected String doInBackground(Intent... params) {
			String body = null;
			HttpResponse response = RESTService.request(params[0]);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				try {
					body = EntityUtils.toString(response.getEntity());
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}				
			}

			return body;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				try {
					JSONObject jsonResponse = (JSONObject) new JSONTokener(result).nextValue();
					if (jsonResponse.getString("status").equals("Success")) {
						Toast.makeText(MainActivity.this, R.string.ip_exception_successful, Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(MainActivity.this, R.string.ip_exception_failure, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int position) {
		switch (position) {
		case 0:
			mCurrentPage = 0;
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
			break;
		case 1:
			mCurrentPage = 1;
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			break;
		default:
			getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		}
	}

	@Override
	public void onPageScrollStateChanged(int state) {}

	class FavoriteTeamsAdapter extends CursorAdapter {

		private final LayoutInflater mInflater;
		private int colTeamName;

		public FavoriteTeamsAdapter(Context context, Cursor c, int flags) {
			super(context, c, flags);
			this.mInflater = LayoutInflater.from(context);
		}

		private class ViewHolder {
			public TextView tvTeam;
			public ToggleButton tbFav;
		}

		@Override
		public void bindView(View view, Context context, Cursor c) {
			ViewHolder holder = (ViewHolder) view.getTag();
			String name = c.getString(colTeamName);
			holder.tvTeam.setText(name);
			holder.tbFav.setTag(name);
			holder.tbFav.setChecked(true);
			holder.tbFav.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (!isChecked) {
						DBUtils.deleteTeam(MainActivity.this, (String) buttonView.getTag());
					}
				}
			});
		}

		@Override
		public Cursor swapCursor(Cursor newCursor) {
			if (newCursor != null) {
				colTeamName = newCursor.getColumnIndexOrThrow(HSDroidContract.Teams.COLUMN_NAME_TEAM_NAME);
			}
			return super.swapCursor(newCursor);
		}

		@Override
		public View newView(Context context, Cursor c, ViewGroup parent) {
			final View view = mInflater.inflate(R.layout.menu_fav_item, parent, false);
			final ViewHolder holder = new ViewHolder();
			holder.tvTeam = (TextView) view.findViewById(R.id.team_name);
			holder.tbFav = (ToggleButton) view.findViewById(R.id.toggle);
			view.setTag(holder);
			return view;
		}
	}

	@Override
	public void onLoadFinished(android.content.Loader<Cursor> loader, Cursor newCursor) {
		mFavoriteTeamsAdapter.swapCursor(newCursor);

	}

	@Override
	public void onLoaderReset(android.content.Loader<Cursor> loader) {
		mFavoriteTeamsAdapter.swapCursor(null);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		Loader<Cursor> cursorLoader = new CursorLoader(this, HSDroidContract.Teams.CONTENT_URI, null, null, null, null);

		return cursorLoader;
	}

	private void setupNotificationServiceAlarm() {
		AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(this, OnAlarmReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);

		mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 15000, PERIOD, pi);

		Intent iClearNotifications = new Intent(this, OnClearNotificationsReceiver.class);
		PendingIntent piClearNotifications = PendingIntent.getBroadcast(this, 0, iClearNotifications, 0);

		mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 5000, PERIOD, piClearNotifications);
	}
}
