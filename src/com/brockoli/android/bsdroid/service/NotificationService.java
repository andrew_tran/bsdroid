package com.brockoli.android.bsdroid.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.brockoli.android.bsdroid.LoginActivity;
import com.brockoli.android.bsdroid.R;
import com.brockoli.android.bsdroid.datamodel.LiveStream;
import com.brockoli.android.bsdroid.utils.DBUtils;

public class NotificationService extends WakefulIntentService {
	private static final String TAG = WakefulIntentService.class.getSimpleName();
	
	public static final String CLEAR_NOTIFICATIONS = "CLEAR_NOTIFICATIONS";
	public static final String SEND_NOTIFICATIONS = "SEND_NOTIFICATIONS";

	public static final String LIVESTREAM = "LIVESTREAM";
	public static final String ALL_STREAMS = "ALL_STREAMS";

	public static final String GAME_COUNT = "GAME_COUNT";


	private String mUsername;
	private String mPassword;
	private List<LiveStream> mStreams;

	public NotificationService() {
		super(TAG);
	}

	@Override
	void doWakefulWork(Intent intent) {
		if (intent.hasExtra(CLEAR_NOTIFICATIONS)) {
			int num = DBUtils.deleteOldNotifications(this);
			Log.d(TAG, "Old notifications cleared: " + num);
		} else if (intent.hasExtra(SEND_NOTIFICATIONS)) {
			mStreams = new ArrayList<LiveStream>();
			// Login
			login();			
		}
	}

	private void login() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		if (prefs.contains(LoginActivity.USERNAME)) {
			mUsername = prefs.getString(LoginActivity.USERNAME, "");
		}
		if (prefs.contains(LoginActivity.PASSWORD)) {
			mPassword = prefs.getString(LoginActivity.PASSWORD, "");
		}  	

		new AsyncLogin().execute(mUsername, mPassword, getString(R.string.apikey));

	}

	private void compareWithFavorites() {
		ArrayList<LiveStream> favoriteStreams = new ArrayList<LiveStream>();
		ArrayList<String> notifyStreams = new ArrayList<String>();
		for (LiveStream stream : mStreams) {
			if (DBUtils.teamExists(this, stream.getAwayTeam()) || DBUtils.teamExists(this, stream.getHomeTeam())) {
				Log.d(TAG, "Found favorite team playing today!");
				if (!DBUtils.notificationExists(this, stream.getId())) {
					DBUtils.insertNotification(this, stream.getId());
					notifyStreams.add(stream.getId());
				}
				favoriteStreams.add(stream);
			}
		}
		scheduleNotification(favoriteStreams, notifyStreams);					
	}
	
	private void scheduleNotification(ArrayList<LiveStream> streams, ArrayList<String> notifyStreams) {
		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		for (LiveStream stream : streams) {
			if ((!stream.getStartTime().equalsIgnoreCase("Final")) && (notifyStreams.contains(stream.getId()))) {
				String startTime = stream.getStartTime();
				DateTime startDateTime = fromStartTime(startTime);
				
				int id = Integer.parseInt(stream.getId());
				Intent intent = new Intent(this, GameTimeAlarm.class);
				intent.putExtra(LIVESTREAM, stream);
				intent.putExtra(ALL_STREAMS, streams);
				PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
				alarmManager.set(AlarmManager.RTC, startDateTime.getMillis(), pendingIntent);									
			}
		}
	}
	
	private DateTime fromStartTime(String startTime) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("h:mm aa z");
		
		LocalTime time = fmt.withOffsetParsed().parseLocalTime(startTime);
		LocalDate date = new LocalDate();
		
		DateTime combinedDateTime = date.toDateTime(time);
		Log.d(TAG, combinedDateTime.toString());
		return combinedDateTime;
		
	}
	/**
	 * AsyncTask to handle a login request
	 * @author brockoli
	 *
	 */
	private class AsyncLogin extends AsyncTask<String, Void, String> {

		/**
		 * Had an idea to use an Intent here to pass the REST parameters to my REST singleton class
		 * Reasoning is that if I decide later to swap in an IntentService instead I don't have to change
		 * much on the front end.
		 */
		@Override
		protected String doInBackground(String... params) {
			Intent intent = new Intent();
			intent.setData(Uri.parse(getString(R.string.rest_method_login)));

			Bundle bundle = new Bundle();
			bundle.putInt(RESTService.EXTRA_HTTP_VERB, RESTService.POST);

			Bundle extras = new Bundle();
			extras.putString("username", params[0]);
			extras.putString("password", params[1]);
			extras.putString("key", params[2]);

			bundle.putBundle(RESTService.EXTRA_PARAMS, extras);
			intent.putExtras(bundle);

			HttpResponse response = RESTService.request(intent);
			if (response != null) {
				String body = null;
				try {
					body = EntityUtils.toString(response.getEntity());
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				return body;				
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				Boolean success = false;

				try {
					JSONObject jsonResponse = (JSONObject) new JSONTokener(result).nextValue();
					if (jsonResponse.getString("status").equals("Success")) {
						success = true;
						String sessionId = jsonResponse.getString("token");
						String favTeam = jsonResponse.getString("favteam");
						String membership = jsonResponse.getString("membership");
						RESTService restService = RESTService.instance();
						restService.setSessionId(sessionId);
						restService.setFavTeam(favTeam);
						restService.setMembership(membership);
					}				} catch (JSONException e) {
						e.printStackTrace();
					}
				if (success) {
					Log.d(TAG, "Service login successful!");
					// Get live games
					new LiveStreamsAsyncTask().execute(RESTService.instance().createRestIntent(RESTService.REST_METHOD_GET_LIVE, null, RESTService.GET));

					// Schedule notification for each game that matches a fav team
				}				
			}
		}
	}

	private class LiveStreamsAsyncTask extends AsyncTask<Intent, Void, String> {

		@Override
		protected String doInBackground(Intent... params) {
			String body = null;
			HttpResponse response = RESTService.request(params[0]);
			if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				try {
					body = EntityUtils.toString(response.getEntity());
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}				
			}

			return body;
		}

		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				try {
					JSONObject jsonResponse = (JSONObject) new JSONTokener(result).nextValue();
					if (jsonResponse.getString("status").equals("Success")) {
						JSONArray jsonStreams = jsonResponse.getJSONArray("schedule");
						for (int i=0; i < jsonStreams.length(); i++) {
							JSONObject jsonStream = jsonStreams.getJSONObject(i);
							String id = jsonStream.getString("id");
							String event = jsonStream.getString("event");
							String homeTeam = jsonStream.getString("homeTeam");
							String awayTeam = jsonStream.getString("awayTeam");
							String startTime = jsonStream.getString("startTime");
							int playing = jsonStream.getInt("isPlaying");
							Boolean isPlaying = (playing == 0) ? false : true;
							int flash = jsonStream.getInt("isFlash");
							Boolean isFlash = (flash == 0) ? false : true;
							int wmv = jsonStream.getInt("isWMV");
							Boolean isWmv = (wmv == 0) ? false : true;
							int istream = jsonStream.getInt("isiStream");
							Boolean isIStream = (istream == 0) ? false : true;
							int hd = jsonStream.getInt("isHd");
							Boolean isHd = (hd == 0) ? false : true;
							LiveStream liveStream = new LiveStream(id, event, homeTeam, awayTeam, 
									isFlash, isWmv, isIStream, startTime, isPlaying, isHd);
							mStreams.add(liveStream);
						}
					}				
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			// Compare live games teams against fav teams from database
			Log.d(TAG, "Service retreived live streams!");
			compareWithFavorites();
		}
	}
}
